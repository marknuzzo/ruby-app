# frozen_string_literal: true

require_relative "app/version"

module Ruby
  # This is a test app
  module App
    class Error < StandardError; end

    def self.run!
      # executing app...

      true
    end
  end
end
