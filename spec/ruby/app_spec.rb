# frozen_string_literal: true

RSpec.describe Ruby::App do
  it "has a version number" do
    expect(Ruby::App::VERSION).not_to be nil
  end

  describe "run!" do
    it "runs the app" do
      expect(Ruby::App.run!).to be_truthy
    end
  end
end
